import { createApp } from "vue";
import ElementPlus from "element-plus";
import "element-plus/lib/theme-chalk/index.css";
import { router } from "./router";
import { store } from "./store";
import App from "./App.vue";

const app = createApp(App);
app.use(router).use(store);
app.use(ElementPlus);

app.mount("#app");
