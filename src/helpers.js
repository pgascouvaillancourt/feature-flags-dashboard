const objectToQueryString = (params) =>
  params
    ? Object.entries(params).reduce(
        (queryString, [key, value]) => (queryString += `&${key}=${value}`),
        "?"
      )
    : "";

export const buildRequest = (endpoint, params = null) =>
  new Request(
    import.meta.env.VITE_API_ROOT + endpoint + objectToQueryString(params),
    {
      method: "GET",
    }
  );
